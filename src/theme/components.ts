import type { ThemeOptions } from "@mui/material/styles";

/**
 * Style overrides for Material UI components.
 * 
 * @see https://mui.com/customization/theme-components/#global-style-overrides
 */
export const components: ThemeOptions["components"] = {
  MuiButton: {
    styleOverrides: {
      contained: {
        boxShadow: "none",
        "&:hover": {
          boxShadow: "none",
        },
      },
    },
  },
  MuiButtonGroup: {
    styleOverrides: {
      root: {
        boxShadow: "none",
      },
    },
  },
};