import { Box, Button, SvgIcon, SvgIconProps, Typography } from '@mui/material';
import { experimental_sx as sx, styled } from '@mui/material/styles';

const Group = styled(Box)(
	sx({
		mb: 1,
		'& .MuiButton-root': {
			m: 1,
		},
	})
);

function LightBulbIcon(props: SvgIconProps) {
	return (
		<SvgIcon {...props}>
			<path d="M9 21c0 .55.45 1 1 1h4c.55 0 1-.45 1-1v-1H9v1zm3-19C8.14 2 5 5.14 5 9c0 2.38 1.19 4.47 3 5.74V17c0 .55.45 1 1 1h6c.55 0 1-.45 1-1v-2.26c1.81-1.27 3-3.36 3-5.74 0-3.86-3.14-7-7-7zm2.85 11.1l-.85.6V16h-4v-2.3l-.85-.6C7.8 12.16 7 10.63 7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 1.63-.8 3.16-2.15 4.1z" />
		</SvgIcon>
	);
}

function PreviewButton() {
	return (
		<Box mt={2}>
			<Typography variant="h6" id="contained-buttons">
				Contained Buttons
			</Typography>
			<Group>
				<Button variant="contained">Default</Button>
				<Button variant="contained" color="primary">
					Primary
				</Button>
				<Button variant="contained" color="secondary">
					Secondary
				</Button>
				<Button variant="contained" disabled>
					Disabled
				</Button>
				<Button variant="contained" color="primary" href="#contained-buttons">
					Link
				</Button>
			</Group>

			<Typography variant="h6" id="text-buttons">
				Text Buttons
			</Typography>
			<Group>
				<Button>Default</Button>
				<Button color="primary">Primary</Button>
				<Button color="secondary">Secondary</Button>
				<Button disabled>Disabled</Button>
				<Button color="primary" href="#text-buttons">
					Link
				</Button>
			</Group>

			<Typography variant="h6" id="outlined-buttons">
				Outlined Buttons
			</Typography>
			<Group>
				<Button variant="outlined">Default</Button>
				<Button variant="outlined" color="primary">
					Primary
				</Button>
				<Button variant="outlined" color="secondary">
					Secondary
				</Button>
				<Button variant="outlined" disabled>
					Disabled
				</Button>
				<Button variant="outlined" color="primary" href="#outlined-buttons">
					Link
				</Button>
			</Group>

			<Typography variant="h6" id="buttons-with-icons">
				Buttons with icons and label
			</Typography>
			<Group>
				<Button variant="contained" color="secondary" startIcon={<LightBulbIcon />}>
					Delete
				</Button>
				<Button variant="contained" color="primary" startIcon={<LightBulbIcon />}>
					Upload
				</Button>
				<Button variant="contained" disabled color="secondary" startIcon={<LightBulbIcon />}>
					Talk
				</Button>
				<Button variant="contained" color="primary" size="small" startIcon={<LightBulbIcon />}>
					Save
				</Button>
				<Button variant="contained" color="primary" size="large" startIcon={<LightBulbIcon />}>
					Save
				</Button>
			</Group>
		</Box>
	);
}

export default PreviewButton;
